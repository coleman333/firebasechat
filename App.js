import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View, TouchableOpacity, AsyncStorage} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import { createStackNavigator, createAppContainer, createSwitchNavigator} from 'react-navigation';
import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import ChatScreen from "./screens/ChatScreen";
import ProfileScreen from "./screens/ProfileScreen";

const AppStack = createStackNavigator({Home: HomeScreen, Chat: ChatScreen, Profile: ProfileScreen });
const AuthStack = createStackNavigator({Login: LoginScreen});


export default createAppContainer(
    createSwitchNavigator(
        {
            AuthLoading:AuthLoadingScreen,
            App: AppStack,
            Auth: AuthStack
        }
    )
)

// // type Props = {};
// export default class App extends Component<Props> {
//
//   state = {
//     phone: '',
//     name: ''
//   };
//
//   handleChange = key => val => {
//     this.setState({ [key]: val })
//   };
//
//   async componentWillMount(): void {
//     const userPhone = await AsyncStorage.getItem('userPhone');
//     // if(userPhone){
//       this.setState({ phone: userPhone });
//     // }
//   }
//
//   submitForm = async() => {
//     if(this.state.phone.length < 10 && this.state.phone.length > 12){
//       alert('wrong phone number');
//     }
//     else if(this.state.name.length < 3){
//       alert('wrong phone name');
//     }
//     else{
//       alert(`${this.phone} \n  ${this.state.name}`);
//     }
//     await AsyncStorage.setItem('userPhone', this.state.phone);
//
//   };
//
//   render() {
//     return (
//       <View style={ styles.container }>
//         <TextInput placeholder={'Phone number'}
//           style={ styles.input }
//           value={ this.state.phone }
//           onChangeText={ this.handleChange('phone') }/>
//
//         <TextInput placeholder={'name'}
//           style={styles.input}
//           value={ this.state.name }
//           onChangeText={ this.handleChange('name') }/>
//         <TouchableOpacity style={styles.buttonContainer} onPress={this.submitForm}>
//           <Text style={styles.buttonStyle}>Enter</Text>
//         </TouchableOpacity>
//       </View>
//     );
//   }
// }
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   input:{
//     padding: 10,
//     borderWidth: 1,
//     borderColor: '#ccc',
//     width: '90%',
//     marginBottom: 10,
//     borderRadius: 10
//   },
//   buttonStyle:{
//     padding: 10,
//     borderWidth: 1,
//     borderColor: 'gray',
//     width: '50%',
//     marginBottom: 10,
//     borderRadius: 10,
//     justifyContent: 'flex-end'
//   },
//   buttonContainer:{
//     display: 'flex'
//   }
//
// });
