import React from 'react';
import {StyleSheet, Text, TextInput, View, TouchableOpacity, Dimensions, AsyncStorage, FlatList, SafeAreaView} from 'react-native';
import firebase from 'firebase';
import User from '../User';

export default class ChatScreen extends React.Component{
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('name', null)
        }
    };

    state = {
        textMessage: '',
        person:{
            name: this.props.navigation.getParam('name'),
            phone: this.props.navigation.getParam('phone')
        },
        messageList:[]

    };

    componentWillMount(): void {
        this.setState({person: {
                name: this.props.navigation.getParam('name'),
                phone: this.props.navigation.getParam('phone')
            }});
        firebase.database().ref('messages').child(User.phone).child(this.state.person.phone)
            .on('child_added',(value) =>{
                this.setState((prevState)=>{
                return {
                    messageList: [...prevState.messageList, value.val()]
                }
            })
            })
    }

    handleChange = key => value => {
        this.setState({[key]: value})
    };

    sendMessage = () => {
        if(this.state.textMessage.length > 0){
            let messageId = firebase.database().ref('messages').child(User.phone).child(this.state.person.phone).push().key;
            let updates = {};
            let message = {
                message: this.state.textMessage,
                time: firebase.database.ServerValue.TIMESTAMP,
                from: User.phone
            };
             updates['messages/' + User.phone+ '/' +this.state.person.phone+ '/' + messageId] = message;
             updates['messages/' + this.state.person.phone + '/' + User.phone + '/' + messageId] = message;
             firebase.database().ref().update(updates);
             this.setState({ textMessage: '' });
        }
    };

    convertTime = (time)=> {
        let d = new Date(time);
        let c = new Date();
        let result = (d.getHours() < 10 ? '0' : '') + d.getHours() + ':';
        result += (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
        // if(c.getDay() !== d.getDay()){
        //     result = d.getDay() + ' ' + d.getMonth() + ' ' + result;
        // }
        return result;
    };

    renderRow = (item) => {
      // this.setState({item});
      //   console.log('ddddddddddddddddddddd', item.item);
        return (
            <View style={{flexDirection: 'row', width: '80%',
            alignItems: item.item.from === User.phone ? 'flex-end' : 'flex-start',
            justifyContent: item.item.from === User.phone ? 'flex-end' : 'flex-start',
            backgroundColor: item.item.from === User.phone ? '#00897b': '#7cb342',
            borderRadius: 5,
            marginBottom: 10}}
            >
                <Text style={{color: '#fff', padding: 7, fontSize: 16}}>
                    {item.item.message}
                </Text>
                <Text style={{color: '#eee', padding: 3, fontSize: 12}}>
                    {this.convertTime(item.item.time)}
                </Text>
            </View>
        )
    };

    render(): React.ReactNode {
        // let { height, width } = Dimensions.get(window);
        const height = Dimensions.get('window').height;
        // const item = this.state.item;
        // console.log('lllllllllllllllllllllllll', item, 'fffff', this.state.item);
        return(
            <SafeAreaView>
                <FlatList
                    style={{
                        padding: 10,
                        height: height * 0.7,
                        // justifyContent: 'flex-end' ,

                    }}
                    data={this.state.messageList}
                    renderItem={this.renderRow}
                    keyExtractor={(item, index)=> index.toString()}
                />
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <TextInput placeholder={'your message'}
                               style={ styles.input }
                               value={ this.state.textMessage }
                               onChangeText = { this.handleChange('textMessage') }/>

                    <TouchableOpacity style={styles.buttonContainer} onPress={this.sendMessage}>
                        <Text style={styles.buttonStyle}>Send</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        width: '100%'
    },
    input:{
        padding: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        width: '75%',
        marginRight: 10,
        marginLeft: 10,

        borderRadius: 10
    },
    buttonStyle:{
        padding: 10,
        borderWidth: 1,
        borderColor: 'gray',
        width: '50%',
        marginBottom: 10,
        borderRadius: 10,
        justifyContent: 'flex-end'
    },
    buttonContainer:{
        display: 'flex',
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems:'center',
        width:'30%'
    },
    cloudMessageStyle:{
        marginBottom: 5,
        marginTop: 5,
        borderRadius: 10,
        borderColor: 'black',
        borderWidth: 1,
        width: 300,
        height: 60
    }

});
