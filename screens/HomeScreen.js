import React from 'react';
import {StyleSheet, Text, TextInput, View, TouchableOpacity, AsyncStorage, FlatList,
    SafeAreaView, Image} from 'react-native';
import User from '../User';
import UserIcon from '../Images/user.png';
import firebase from 'firebase';

export default class HomeScreen extends React.Component {

    static navigationOptions = ({navigation}) =>{
        return {
            title: 'Chats',
            headerRight: (
              <TouchableOpacity onPress={()=>{navigation.navigate('Profile') } }>
                  <Image style={{height: 32 , width: 32, marginRight: 10}} source={UserIcon}/>
              </TouchableOpacity>
            )
        }
    };

    state = {
        users:[],
    };

    componentWillMount(): void {

        let db = firebase.database().ref('Users');
        db.on('child_added', (value)=>{
            let person = value.val();
            person.phone = value.key;
            if(person.phone === User.phone){
                User.name = person.name
            }
            else{
                this.setState((prevState)=>{
                    return {users: [...prevState.users, person] }
                })
            }

        })
    }

    renderRow = ({item})=>{
        return(<TouchableOpacity
            onPress={()=>this.props.navigation.navigate('Chat',item)}
            style={styles.cloudMessageStyle} >
            <Text >
                {item.name}
            </Text>
        </TouchableOpacity>)
    };

    render() {
        return (
            <SafeAreaView style={ styles.container }>
                <Text style={{justifyContent: 'center'}}>{User.phone}</Text>
                 <FlatList
                    data={this.state.users}
                    renderItem={this.renderRow}
                    keyExtractor={(item)=>item.phone}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        width: '100%'
    },
    input:{
        padding: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        width: '90%',
        marginBottom: 10,
        borderRadius: 10
    },
    buttonStyle:{
        padding: 10,
        borderWidth: 1,
        borderColor: 'gray',
        width: '50%',
        marginBottom: 10,
        borderRadius: 10,
        justifyContent: 'flex-end'
    },
    buttonContainer:{
        display: 'flex'
    },
    cloudMessageStyle:{
        marginBottom: 5,
        marginTop: 5,
        borderRadius: 10,
        borderColor: 'black',
        borderWidth: 1,
        width: 300,

        height: 60

    }


});
