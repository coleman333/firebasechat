import React from "react";
import { View, AsyncStorage, ActivityIndicator, StatusBar} from 'react-native';
import User from '../User';
import firebase from 'firebase';


export default class AuthLoadingScreen extends React.Component{
    constructor(props){
        super(props);
        // this._bootstrapAsync();
    }

    componentWillMount(): void {
        this._bootstrapAsync();
        var config = {
            apiKey: "AIzaSyCqXUJ0zIenWYM3wi4Y7aMXc6qBwlJtyf4",
            authDomain: "fir-chat-16bac.firebaseapp.com",
            databaseURL: "https://fir-chat-16bac.firebaseio.com",
            projectId: "fir-chat-16bac",
            storageBucket: "fir-chat-16bac.appspot.com",
            messagingSenderId: "758307177419"
        };
        firebase.initializeApp(config);
    }

    _bootstrapAsync = async () => {
        console.log('-----------------');
        User.phone = await AsyncStorage.getItem('userPhone');
        this.props.navigation.navigate(User.phone ? 'App' : 'Auth');
    }

    render(){
        return(
            <View>
                <ActivityIndicator/>
                <StatusBar barStyle={'default'}/>
            </View>
        )
    }

}
