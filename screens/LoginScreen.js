import React from 'react';
import {StyleSheet, Text, TextInput, View, TouchableOpacity, AsyncStorage} from 'react-native';
import User from '../User';
import firebase from 'firebase';

export default class LoginScreen extends React.Component {

    state = {
        phone: '',
        name: ''
    };

    static navigationOptions = {
        // tabBarLabel: 'Login'
        header: null
    };

    handleChange = key => val => {
        this.setState({ [key]: val })
    };

    submitForm = async() => {
        // if(this.state.phone.length < 10 && this.state.phone.length > 12){
        //     alert('wrong phone number');
        // }
        // else if(this.state.name.length < 3){
        //     alert('wrong phone name');
        // }
        // else{
            // alert(`${this.phone} \n  ${this.state.name}`);
            await AsyncStorage.setItem('userPhone', this.state.phone);
            User.phone = this.state.phone;
            firebase.database().ref('Users/' + User.phone).set({name: this.state.name});
            this.props.navigation.navigate('App');
        // }


    };

    render() {
        return (
            <View style={ styles.container }>
                <TextInput placeholder={'Phone number'}
                           style={ styles.input }
                           value={ this.state.phone }
                           onChangeText={ this.handleChange('phone') }/>

                <TextInput placeholder={'name'}
                           style={styles.input}
                           value={ this.state.name }
                           onChangeText={ this.handleChange('name') }/>
                <TouchableOpacity style={styles.buttonContainer} onPress={this.submitForm}>
                    <Text style={styles.buttonStyle}>Enter</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    input:{
        padding: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        width: '90%',
        marginBottom: 10,
        borderRadius: 10
    },
    buttonStyle:{
        padding: 10,
        borderWidth: 1,
        borderColor: 'gray',
        width: '50%',
        marginBottom: 10,
        borderRadius: 10,
        justifyContent: 'flex-end'
    },
    buttonContainer:{
        display: 'flex'
    }

});
