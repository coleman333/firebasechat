import React from 'react';
import { View, Text, SafeAreaView, TextInput, TouchableOpacity, StyleSheet, Alert, AsyncStorage } from 'react-native';
import User from '../User';
import firebase from 'firebase';

export default class ProfileScreen extends React.Component{

  static navigationOptions ={
    title: 'Profile'
  };

  state={
    phone: '',
    name: User.name,
  };

  handleName =key => value =>{
    this.setState({[key]: value})
  };

  changeName =()=>{
    if(this.state.name < 3){
      Alert.alert('The name must be at least 5 simbols');
    }else {
      if (User.name !== this.state.name) {
        firebase.database().ref('users').child(User.phone).set({ name: this.state.name });
        User.name = this.state.name;
        Alert.alert('name changed successfully');
      }
    }
  };

  logout = async() => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };

  render(){
    return (
      <SafeAreaView style={{justifyContent:'center', alignItems:'center'}}>
        <Text style={{fontSize: 20}}>
          {User.phone}
        </Text>
        <Text style={{fontSize: 20}}>
          {User.name}
        </Text>
        <TextInput
          style={{  padding: 10,
            borderWidth: 1,
            borderColor: '#ccc',
            width: '75%',
            marginRight: 10,
            marginLeft: 10,
            borderRadius: 10
          }}
          onChangeText={this.handleName('name')}
          value={this.state.name}
        />
        <TouchableOpacity style={styles.buttonContainer} onPress={this.changeName}>
          <Text style={styles.buttonStyle}>Change name</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonContainer} onPress={this.logout}>
          <Text style={styles.buttonStyle}>Logout</Text>
        </TouchableOpacity>

      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    width: '100%'
  },
  input:{
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    width: '75%',
    marginRight: 10,
    marginLeft: 10,

    borderRadius: 10
  },
  buttonStyle:{
    padding: 10,
    borderWidth: 1,
    borderColor: 'gray',
    width: '100%',
    marginBottom: 10,
    borderRadius: 10,
    justifyContent: 'flex-end'
  },
  buttonContainer:{
    display: 'flex',
    flexDirection:'row',
    justifyContent: 'flex-start',
    alignItems:'center',
    width:'30%'
  },
  cloudMessageStyle:{
    marginBottom: 5,
    marginTop: 5,
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    width: 300,
    height: 60
  }

});